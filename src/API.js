const API_KEY = '5e38ea2de7590db8cd598cba52b8634df39d7c3ef4cf523b0cca74201b31c26c';

export const loadCoinsList = () => fetch(
    `https://min-api.cryptocompare.com/data/blockchain/list?api_key=${API_KEY}`
).then(r => {
    return r.json()
});

const tickersHandlers = new Map();

const socket = new WebSocket(
    `wss://streamer.cryptocompare.com/v2?api_key=${API_KEY}`
);

const AGGREGATE_INDEX = "5";
const ERROR_CODE = "500";
const INVALID_SUB = "INVALID_SUB";

socket.addEventListener("message", e => {
    const { TYPE: type, FROMSYMBOL: currency, PRICE: newPrice, MESSAGE: message, INFO: info, PARAMETER: parameter } = JSON.parse(
        e.data
    );
    if (type === ERROR_CODE && message === INVALID_SUB) {
        const params = parameter.split('~');
        const coin = params[2];
        const currency = params[3];

        console.log(coin, currency, e);
        console.log(message, info);
    }
    if (type !== AGGREGATE_INDEX || newPrice === undefined) {
        return;
    }

    const handlers = tickersHandlers.get(currency) ?? [];
    handlers.forEach(fn => fn(newPrice));
});

function sendToWebSocket(message) {
    const stringifiedMessage = JSON.stringify(message);

    if (socket.readyState === WebSocket.OPEN) {
        socket.send(stringifiedMessage);
        return;
    }

    socket.addEventListener(
        "open",
        () => {
            socket.send(stringifiedMessage);
        },
        { once: true }
    );
}


function subscribeToTickerOnWs(ticker, currency = 'USD') {
    sendToWebSocket({
        action: "SubAdd",
        subs: [`5~CCCAGG~${ticker}~${currency}`]
    });
}

function unsubscribeFromTickerOnWs(ticker) {
    sendToWebSocket({
        action: "SubRemove",
        subs: [`5~CCCAGG~${ticker}~USD`]
    });
}

export const subscribeToTicker = (ticker, cb) => {
    const subscribers = tickersHandlers.get(ticker) || [];
    tickersHandlers.set(ticker, [...subscribers, cb]);
    subscribeToTickerOnWs(ticker);
};

export const unsubscribeFromTicker = ticker => {
    tickersHandlers.delete(ticker);
    unsubscribeFromTickerOnWs(ticker);
};

